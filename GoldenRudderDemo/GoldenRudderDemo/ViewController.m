//
//  ViewController.m
//  GoldenRudderDemo
//
//  Created by toby on 16/8/17.
//  Copyright © 2016年 toby. All rights reserved.
//

#import "ViewController.h"

#import <GoldenRudderFramework/GoldenRudderFramework.h>

@interface ViewController () <GRPluginViewControllerDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [GRPluginParma setLogEnabled:YES];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    UINavigationBar *bar = self.navigationController.navigationBar;
    bar.backgroundColor = [UIColor redColor];
    
    bar.translucent = false;
    UIImage *image = nil;[UIImage imageNamed:@"qwe"];
    if ([bar respondsToSelector:@selector(setBackgroundImage:forBarPosition:barMetrics:)]) {
        [self.navigationController.navigationBar setBackgroundImage:nil forBarPosition:UIBarPositionTopAttached barMetrics:UIBarMetricsDefault];
    }else{
        if ([bar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)]) {
            [bar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionEnterPayWithButton:(UIButton *)sender {
    
    GRData *data = [[GRData alloc] init];
    data.pId = @"990001366416";
    data.cId = @"18600908987";
    data.houseType = GRHouseTypeOne;
    data.productType = GRProductTypeCommon;
    data.speed = 20;
    
    GRPluginViewController *vc = [[GRPluginViewController alloc] initData:data];
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) payResultsWithInfo:(GRPayResultsStatus) status {
    NSLog(@"%d", status);
}

- (BOOL) didBeginPay {
    NSLog(@"didBeginPay");
    return YES;
}

- (BOOL) payResultsIsExitVC {
    return YES;
}

@end
