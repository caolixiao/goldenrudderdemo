//
//  AppDelegate.h
//  GoldenRudderDemo
//
//  Created by toby on 16/8/17.
//  Copyright © 2016年 toby. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

