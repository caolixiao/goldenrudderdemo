//
//  GRPluginViewController.h
//  GoldenRudderFramework
//
//  Created by toby on 16/5/23.
//  Copyright © 2016年 toby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GRPluginViewControllerDelegate.h"
#import "GRData.h"


@interface GRPluginViewController : UIViewController

@property (nonatomic, assign) id<GRPluginViewControllerDelegate> delegate;

/**
 * @brief 调用支付界面
 *
 * @param data 支付信息
 *
 * @return 返回初始化的对象
 */
- (instancetype) initData:(GRData *) data NS_AVAILABLE_IOS(8_0);

@end

