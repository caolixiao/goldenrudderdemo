//
//  GRData.h
//  GoldenRudderFramework
//
//  Created by toby on 16/7/21.
//  Copyright © 2016年 toby. All rights reserved.
//

#import <Foundation/Foundation.h>

// 支付结果
typedef NS_ENUM(int, GRPayResultsStatus) {
    GRPayResultsStatusNone  = 0, // 未支付
    GRPayResultsStatusSuc   = 1, // 支付成功
    GRPayResultsStatusFail  = 2  // 支付失败
};

// 套餐适合的房屋类型
typedef NS_ENUM(int, GRHouseType) {
    GRHouseTypeNone     = 0, // 未知
    GRHouseTypeOne      = 1, // 1、整租
    GRHouseTypeTwo      = 2, // 2、合租
};

// 产品类型
typedef NS_ENUM(NSInteger, GRProductType) {
    GRProductTypeCommon     = 1, // 通用
    GRProductTypeExperience = 2, // 体验
};


@interface GRData : NSObject

@property (nonatomic,strong) NSString *pId;             // 母账号
@property (nonatomic,strong) NSString *cId;             // 子账号

@property (nonatomic,assign) GRHouseType houseType;     // 房屋类型
@property (nonatomic,assign) GRProductType productType; // 产品类型

@property (nonatomic,assign) int speed;                 // 产品速率标识 2M、4M、8M

@end
