//
//  GRPluginParma.h
//  GoldenRudderFramework
//
//  Created by toby on 16/7/21.
//  Copyright © 2016年 toby. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GRPluginParma : NSObject

/**
 * 使用测试服务器
 *
 */
+ (void)useTest NS_DEPRECATED_IOS(2_0, 5_0, "Use .setBaseURL:  add base url");

/**
 * 设置基础的URL
 */
+ (void) setBaseURL:(NSString *) url;

/**
 * 显示DEBUG日志
 */
+ (void)setLogEnabled:(BOOL)value;


/**
 *  注册微信ID
 */
+ (BOOL) registerWXAppId:(NSString *) appid withDescription:(NSString *)appdesc;

/**
 *  返回支付宝的URL Schemes
 *  @param schemes 默认值为 goldenruddersalipay
 */
+ (void) registerAliURLSchemes:(NSString *) schemes;


/**
 * @return 返回是否进入执行
 */
+ (BOOL) goldenRudderOpenURL:(NSURL *) url;

@end
