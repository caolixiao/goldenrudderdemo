//
//  GoldenRudderFramework.h
//  GoldenRudderFramework
//
//  Created by toby on 16/8/17.
//  Copyright © 2016年 toby. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for GoldenRudderFramework.
FOUNDATION_EXPORT double GoldenRudderFrameworkVersionNumber;

//! Project version string for GoldenRudderFramework.
FOUNDATION_EXPORT const unsigned char GoldenRudderFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <GoldenRudderFramework/PublicHeader.h>


#import <GoldenRudderFramework/GRData.h>
#import <GoldenRudderFramework/GRPluginParma.h>

#import <GoldenRudderFramework/GRPluginViewController.h>


