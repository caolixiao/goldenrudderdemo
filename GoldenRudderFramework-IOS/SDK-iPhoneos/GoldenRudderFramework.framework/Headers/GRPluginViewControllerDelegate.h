//
//  GRPluginViewControllerDelegate.h
//  GoldenRudderFramework
//
//  Created by caolixiao on 16/8/23.
//  Copyright © 2016年 toby. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol GRPluginViewControllerDelegate <NSObject>

@optional
/**
 * @brief 支付结果界面
 * @seek 返回支付结果
 *
 *  @param status 返回支付结果
 **/
- (void) payResultsWithInfo:(GRPayResultsStatus) status;


/**
 * @brief 退出SDK
 * @seek 用户点击返回退出SDK
 *
 **/
- (void) backSDK;

/**
 * @brief 调用支付出发
 * @seek 在开始掉用支付前掉用该接口
 *
 * @return 设置 YES 是进入支付， 设置 NO 这不进行支付 ， 默认为YES
 **/
- (BOOL) didBeginPay;

/**
 * @brief 支付完成以后是否退出SDK
 * @seek 支付完成以后是否退出SDK
 *
 * @return 设置 YES 是退出SDK， 设置 NO 不退出SDK ， 默认为YES
 **/
- (BOOL) payResultsIsExitVC;




@end
