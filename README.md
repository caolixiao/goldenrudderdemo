# 金舵智能宽带sdk集成说明


### 集成方式
+ 第三方工具cocoaPods；
+ 手动添加动态库；

### 集成压缩包内容

+ GoldenRudderFramework.framework : 真机和模拟器合并包，<font color=red>需要通过merge.sh生成</font>
+ SDK-iPhone/GoldenRudderFramework.framework : 动态库真机包
+ SDK-iPhoneSimulator/GoldenRudderFramework.framework : 动态库模拟器包
+ GoldenRudder.bundle : 图片资源文件包，<font color=red>为了和客户app统一风格可以替换里边的图片</font>

### 需要环境支持

+ iPhone 4s及以上设备
+ iOS 8.0 及其之后版本
+ xcode 7.3 及其之后版本<font color=red>(低于此版本不保证能顺利编译)</font>


### SDK使用第三方库

+ [支付宝（ant-open.com）](http://open.alipay.com/index.htm "alipay")
+ [微信支付（微信开发平台）](https://open.weixin.qq.com "wxpay")


### 集成SDK (<font color=red>请设置完整</font>) 
 
##### 静态库真机包 
+ General
	+ Embedded Binaries
		
			GoldenRudderFramework.framework(真机包)
	  
+ Build Phases	 

	+ Copy Bundle Resources

			GoldenRudder.bundle

			  
#####  Info.plist 设置

+ <font color=red>http</font> 以iOS9 SDK编译的工程会默认以SSL安全协议进行网络传输，即HTTPS，如果使用HTTP协议请求网络会报系统异常并中断请求。目前可用如下两种方式保持用HTTP进行网络连接：
	
1. 设置金陀智能宽带SDK http链接<font color=red>（暂时还没有定地址 用???.com 代替）</font>
	
		<key>NSAppTransportSecurity</key>
		<dict>
	    	<key>NSExceptionDomains</key>
	    	<dict>
        		<!-- 金舵智能宽带的HTTP白名单 begin -->
        		<key>???.com</key>
        		<dict>
            		<key>NSIncludesSubdomains</key>
            		<true/>
					<key>NSThirdPartyExceptionAllowsInsecureHTTPLoads</key>
            		<true/>
            		<key>NSThirdPartyExceptionRequiresForwardSecrecy</key>
            		<false/>
	        	</dict>
    	    	<!-- 金舵智能宽带的HTTP白名单 end -->
	    	</dict>
		</dict>
	
2. 在info.plist的NSAppTransportSecurity下新增NSAllowsArbitraryLoads并设置为YES，指定所有HTTP连接都可支持http协议 
	
		<key>NSAppTransportSecurity</key>
		<dict>
    		<key>NSAllowsArbitraryLoads</key>
    		<true/>
		</dict>

3. 在info.plist下增加可跳转的白名单，指定对应跳转App的URL Scheme 否则会拿不到app是否安装状态
        
		<key>LSApplicationQueriesSchemes</key>
    	<array>
			<string>wechat</string>
	     	<string>weixin</string>
        	<string>alipay</string>
    	</array>


4. <font color=red>URLTypes</font> 设置完成后调起app的URLTypes
	
		<key>CFBundleURLTypes</key>
		<array>
			<dict>
				<key>CFBundleTypeRole</key>
				<string>Editor</string>
				<key>CFBundleURLName</key>
				<string>alipay</string>
				<key>CFBundleURLSchemes</key>
				<array>
					<string>goldenruddersalipay</string>
				</array>
			</dict>
			<dict>
				<key>CFBundleTypeRole</key>
				<string>Editor</string>
				<key>CFBundleURLName</key>
				<string>weixin</string>
				<key>CFBundleURLSchemes</key>
				<array>
					<string>您与后台配置的微信appid</string>
				</array>
			</dict>
		</array>


##### 静态库模拟器包 
* 如果您还想在模拟器上运行，请按照下面说明添加配置 Build Setting
 
+ Framework Search Paths
	+ Debug : <font color=red>删除掉真机包路径</font>          
		+ Any iOS Simulator SDK  : <font color=red>添加模拟器包路径,就是把 其中的 值 SDK-iPhoneos 换成 SDK-iPhonesimulator</font>  
		+ Any iOS SDK  :   <font color=red>保持真机包路径</font> 
		
		
+ Runpath Search Paths
	+ Debug : <font color=red>删除掉 @executable_path/Frameworks </font>          
		+ Any iOS Simulator SDK  : <font color=red>添加模拟器包路径,就是把 Framework Search Paths 中的 Any iOS Simulator SDK 对应的 SDK-iPhonesimulator 拷贝过来</font>   
		+ Any iOS SDK  :   <font color=red>保持 @executable_path/Frameworks </font> 
		
		
#### cocoaPods方式

+ Podfile 文件内容
	
		platform :ios, '8.0'
		inhibit_all_warnings!
        
        source 'https://github.com/CocoaPods/Specs.git'        
        source 'https://caolixiao@bitbucket.org/caolixiao/mlspecs.git'

		target "GoldenRudderDemo" do
    
    		xcodeproj 'GoldenRudderDemo/GoldenRudderDemo.xcodeproj'

		    pod 'libGoldenRudder', :git => 'https://caolixiao@bitbucket.org/caolixiao/goldenrudderdemo.git'   
		    
			end

		
		
	
---
# API说明

#### 1. API 的调用

+ 调起SDK界面
	
		#import <GoldenRudderFramework/GoldenRudderFramework.h>
	
	 
    	GRData *data =[[GRData alloc] init];
	    ......
	
		GRPluginViewController *vc = [[GRPluginViewController alloc] initData:data]; 
		vc.delegate = self; // 设置支付结果
		[self.navigationController pushPlayerViewController:vc animated:YES];
		 

#### 2. 设置调起支付包支付完成的回调

+ 回调设置

		- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
		{
		    [GRPluginParma goldenRudderOpenURL:url];
		    return YES;
		}

		// iOS9 以后用
		- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options {
		    [GRPluginParma goldenRudderOpenURL:url];
		    return YES;
		}


#### 3. API 枚举设置

+ 套餐适合的房屋类型 

		typedef NS_ENUM(NSInteger, GRHouseType) {
			GRHouseTypeNone    	=0,	// 未知
    		GRHouseTypeOne 		=1,	// 1、整租
	    	GRHouseTypeTwo 		=2, // 2、合租
		};

+ 产品类型

		typedef NS_ENUM(NSInteger, GRProductType) {
    		GRProductTypeCommon     = 1, // 通用
   			GRProductTypeExperience = 2, // 体验
		};

+ 支付结果
		
		// 支付结果
		typedef NS_ENUM(int, GRPayResultsStatus) {
		    GRPayResultsStatusNone  = 0, // 未支付
		    GRPayResultsStatusSuc   = 1, // 支付成功
		    GRPayResultsStatusFail  = 2  // 支付失败
		};

		
#### 4. GRData 参数设置

+ 设置调起SDK参数
 	 
    	GRData *data =[[GRData alloc] init];
    	
    	data.pId = @"22001197";			 	// (NSString) 母账号
    	data.cId = @"22001197"; 			// (NSString) 子账号
		data.speed = 2;  					// int  产品速率标识 2M、4M、8M
        data.houseType = GRHouseTypeNone;	// (GRHouseType) 房屋类型
        data.productType = GRProductTypeCommon;		// (GRProductType) 产品类型
   
#### 5. 支付结果回调

+ 设置回调方法

		- (void) payResultsWithInfo:(GRPayResultsStatus) status {
		    NSLog(@"%d", status);
		    // TODO: 支付结果处理
		}
		
		- (void) backSDK {
			// TODO: 点击返回按钮返回sdk 的处理
		}
        
        - (void) didBeginPay {
            // TODO: 点击支付按钮是调用
        }

        - (BOOL) payResultsIsExitVC {
            // TODO: 支付完成以后是否退出SDK
        }


#### 6. 注册微信支付appid

+ 注册微信设置

		[GRPluginParma registerWXAppId:@"您的微信appid" withDescription:@"demo 2.0"];
		
#### 7. 支付宝调起app的 URL schemes

+ 注册支付宝设置

		// 默认值为 goldenruddersalipay
		[GRPluginParma registerAliURLSchemes:@"你的 URL Schemes"];


<a name="设置基础的联网地址"></a>
#### 8. 设置基础的联网地址
+ 设置基础的URL
 		
 		[GRPluginParam setBaseURL:@"http://111.com:8081/gene-web/api"];


### 使用测试服务器方式(DEPRECATED)

+ 调用 一下方法  [GRPluginParma useTest]; 即可 前提是对接了测试服务器才行
+ 该方法已废弃 想实现改功能请用 3.7 的方法 [7. 设置基础的联网地址](#设置基础的联网地址)


